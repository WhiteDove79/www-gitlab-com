---
layout: markdown_page
title: "Women at GitLab Mentorship Program"
description: "Mentorship opportunities for women at GitLab sponspored by a partnership between the Women TMRG, GitLab Learning and Development team, and the GitLab DIB team"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Women at GitLab [Mentorship](https://about.gitlab.com/handbook/people-group/learning-and-development/mentor/) Program 

The Women at GitLab Mentorship program is a career development opportunity for participants to build trust across the organization, enable pathways for internal growth and development, and increase collaboration across teams. The program is hosted in collaboration with the Women's TMRG, GitLab Learning and Development team, and the GitLab DIB team.

## History

In 2020, the Women's TMRG partnered with the Sales Organization at GitLab to offer the [Women in Sales Mentorship Program pilot](https://about.gitlab.com/handbook/people-group/women-in-sales-mentorship-pilot-program/#women-in-sales-mentorship-program-pilot). This initial iteration was a huge success and inspired the scale of this iteration to include a wider audeince. 

The [second iteration of this program](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/122) ran during July, August, and September 2021 including 32 mentor/mentee pairs with participation from 16+ departments.

The third iteration of this program is currently running from February to June of 2022. The program currently includes more than 110 participants.

## Testimonials

Here what mentors and mentees at GitLab have to say about the program:

**From the Mentees**

`This was my first mentorship experience and I'm so glad I participated. I found my self confidence in my mentor session. I lacked that for so many years and to be reminded of my self value and worth was so enriching. I would highly recommend participating!`

`I really enjoyed having a mentor-mentee relationship with a Director level team member who was not in my division/department. It made it easier for me to feel safe on discussing pain points and see a different perspective on the business as a whole. I really felt supported and s thought the relationship was important to us both, which was empowering.`

`The openness and vulnerability delivered from my mentor made me feel supported and empowered.`

`This mentorship program has been one of the highlights from my time at GitLab so far. My mentor is a great example of an empathetic leader who is great at strategic planning and communicating effectively. I was able to learn things during the program that I immediately applied to my work, and she inspired and motivated me in a way will persist much longer. My mentor has become one of my new role models, and I plan on keeping in touch with her far beyond this program's end.`

**From the Mentors**

`I really enjoyed being a mentor. It was helpful to establish a relationship with someone outside of my department, because it gave me an opportunity to see how other teams are working. It also encouraged me to reflect on my own career progression.`

`I am really proud of the supportive culture the women of GitLab have created. And this was a valuable opportunity for me to get to take part and contribute. I learned a lot from my mentee that I was able to apply back to management.`

`Determining your objectives out of the program is key to getting value from it. If you don't invest time in preparing you won't have any rewards to reap as a result.`

`One of the elements I really enjoyed about this program was the opportunity to meaningfully connect with someone outside of my team - we both learned from each other!`


## Program Structure

**Initial program kickoff**

A 25-minute discussion pannel with mentors, mentees, and company leadership. Attendance is **required**.

**1:1 Mentor Sessions**

Mentors and mentees should meet every other week for 30-minutes for a total of 5 months (with the option to extend). Sessions are led by the mentee and should be focused on specific goals. Attendance is **required**.

**Mentor/Mentee Workbook**

[Async resources](/handbook/people-group/learning-and-development/mentor/#mentoring-resources) are available as a training guide for all mentor/mentee pairs. These resources include suggested articles to read and discuss, strategies for goal setting, sample meeting agendas, and additional training material. Use of resources is encouraged but not required.

**Mentor/Mentee Training**

Completion of the self-paced [How to be a Good Mentor or Mentee](https://www.linkedin.com/learning/how-to-be-a-good-mentee-and-mentor/the-power-of-mentoring?u=2255073) course on LinkedIn Learning is **required** of all mentors and mentees. If you prefer to review text-based information, you can instead review [this slides](/handbook/people-group/learning-and-development/mentor/#mentor-and-mentee-training), created using content from the LinkedIn course.

**End of Program Celebration**

A 25-minute discussion to reflect on the experience. Attendance is **optional**.


## Current Program Timeline

Review the progam timeline below for the next iteration of this program which will run from February through June of 2022

| Date | Description | Related Resources |
| ----- | ----- | ----- |
| 2021-09 | Discuss program details with Women's TMRG group meeting |  |
| 2021-12-08 | Call for Mentors | |
| 2021-12-08| Call for Mentees | |
| 2022-01-12 | Applications Close | |
| 2022-01-21  | Pairing of mentor relationships complete and communicated to applicants | |
| 2022-01-24 to 2022-02-04 | Mentor/Mentee pre-program coffee chats | |
| 2022-02-2 and 2022-02-3 | [Initial program Kickoff meeting](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/#program-structure) | [Meeting Agenda](https://docs.google.com/document/d/1a6G7wqcfZTtEbMzsN0sdf-s7-GKIE6H2s0J_-XdFduM/edit) |
| 2022-06 | End of program celebration | Exact date TBD |


## Being a Mentor

As a mentor, you benefit from:

- Fast feedback loop between team members and leadership
- Opportunity to form relationships with team members in other departments
- Opportunity to support Women at GitLab and live our values

### Mentor requirements

- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentee on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)
- You can complete the [DIB training certification](https://gitlab.edcast.com/journey/ECL-5c978980-c9f0-4479-bf44-45d44fc56d05) before the program begins

**Applications will be prioritized based on:**

- Skill and prior experience as a mentor
- Performance in current role
- Available and appropriate mentee match in the current session

### Apply

Applications are currently closed as the 3rd program iteration is in progress through June 2022. Please complete [this interest form](https://docs.google.com/forms/d/e/1FAIpQLSc49xN1n3VNzX-yQNbRcG6CJiksQy41aAMw0QnF1F6vyhwgkA/viewform?usp=sf_link) to be notified of the next program iteration. Discover other mentorship programs for team members in the [mentorship handbook](/handbook/people-group/learning-and-development/mentor/#organized-mentorship-programs).

_Applying as a mentor does not guarantee you a spot in the program and eligibility will be evaluated based on criteria above._

If you've participated in a previous GitLab mentor program, you are welcome to apply again!

## Being a Mentee

As a mentee, you benefit from:

- Increased visibility with leadership
- Increased professional development opportunities
- Career coaching and guidance
- Opportunity to form relationships with leaders on other teams

### Mentee requirements

- You're a woman-identifying team member at GitLab. Gender non-conforming folks who face workplace challenges similar to those of women-identifying team members are also welcome to apply.
- You have been with GitLab for at least 3 months at the time of application 
- You have availability in your schedule to meet with your mentor on a bi-weekly basis for at least 30 minute meetings for the entire length of the program
- You plan to make every effort to attend all live trainings included in the program structure
- You are not on a [Formal Coaching plan](/handbook/leadership/underperformance/#options-for-remediation) or PIP (Performance Improvement Plan)

Applications will be prioritized on many points including:

- Team members on a management career track
- Performance in current role

### Apply

Applications are currently closed as the 3rd program iteration is in progress through June 2022. Please complete [this interest form](https://docs.google.com/forms/d/e/1FAIpQLSc49xN1n3VNzX-yQNbRcG6CJiksQy41aAMw0QnF1F6vyhwgkA/viewform?usp=sf_link) to be notified of the next program iteration. Discover other mentorship programs for team members in the [mentorship handbook](/handbook/people-group/learning-and-development/mentor/#organized-mentorship-programs).

_Applying as a mentee does not guarentee you a spot in the program, and eligibility will be evaluated based on criteria above._

If you've participated in a previous GitLab mentor program, you are welcome to apply again!

## Metrics and Outcomes

Mentors and mentees will complete a pre-program and end of program survey to collect feedback, assess skills, and determine if goals set during mentor/mentee relationships have been set.

Success of this program will be measured by:

1. percentage of mentor/mentee pairs who meet consistently on a bi-weekly basis, with a goal of 95% completion
1. attendance to live training sessions, with a goal of 95% attendance
1. quality of feedback of the program from both the mentor and mentee perspective
1. percentage of mentee goal achieved during mentorship, as reported by mentees with a goal of at least 50% of goal achieved
1. mentor and mentee career development at GitLab after program conclusion including rates of promotion, transfer, and retention


## FAQ

**Can I participate as both a mentor and a mentee?**

Yes, this is a possibility. It's important that team members talk with their managers to discuss their capacity to participate in both roles in this program. Participation in both roles will require a 2hr commitment per month (two 30 minute sessions as a mentor and two 30 minute sessions as a mentee).

It's possible that we will have a shortage of mentors and be unable to complete all mentor/mentee pairs. If there is a shortage of mentors and you've applied as both a mentor and a mentee, you will be assigned **only** as a mentor in this program. 

**I participated in a past mentorship program at GitLab. Can I still participate in the current iteration?**

Yes! Past participants in any mentor program and as either mentees, mentors, or both, are encouraged to apply to participate again.

**I'd like to participate but I can't commit the entire length of the program. Can I still apply?**

If you cannot commit to the 3 month program, please consider applying for a future mentorship program at GitLab. It's important that both mentors and mentees are available for the full 3 month program to achieve the best results.

**I'm not eligible to be a mentor or a mentee in this program. What are my options for future participation?**

The L&D team is implementing a  company-wide mentor program in FY23 Q1. Please watch for updates on this iteration.

Team members are always encouraged to build mentor/mentee relationships independently. You can find resources and a list of available mentors in the [Learning and Development handbook](/handbook/people-group/learning-and-development/mentor/)


## Additional questions?

Questions, comments, and feedback can be posted in the [#women](https://app.slack.com/client/T02592416/C7V402V8X) slack channel. 


## 2021 Program Results

### Participation 

1. Total Participants: 66 team members / 33 mentor/mentee pairs
1. Unique Departments Represented by Mentees: 19
1. Unique Departments Represented by Mentors: 18

### Event Attendance

| Event Name | Total Attendance Percentage | Attendance Session 1/Session 2/Session 3 |
| ----- | ----- | ----- |
| Program Kickoff | 95.31% | 7 / 12 / 42 |
| Mid-Program Event | 4.5% | 0 / 0 / 3 |
| End of Program Event | 27% | 4 / 6 / 8 |

**Kick-off session recordings:**

Session 3: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/pR-WPq4uH5o" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Watch [session 1](https://www.youtube.com/embed/cuRT7nUU10Q)and [session 2](https://www.youtube.com/embed/sY57W3xbLJU) on the GitLab Unfiltered YouTube channel.


### Post-Program Survey

Skill development was a key variable used to measure success of this program. 

**Top 5 skills developed by Mentors**

- Situational coaching (73.9%)
- Mentorship (52.2%)
- Listening (47.8%)
- Coaching (43.5%)
- Communication (34.8%)

**Top 5 skills developed by Mentees**

- Communication (78.9%)
- Confidence (63.2%)
- Strategy and idea pitching (36.8%)
- Self-promotion (36.8%)
- Self-value (31.6%)

### Other metrics

- 84.3% of mentees plan to continute meeting with their mentor
- 55% of mentees want to participate as mentors in future iterations of this program
- 91.3% of mentors had the time, resources, and support to be a great mentor
- 86.9% thought their mentee was a good fit based on their career history and expertise

### Pre-Program Survey

#### Mentors

*What skills do you hope this program will help you develop and/or strengthen?*

<div style="text-align: center;display: block;">
<img class="benImg" src="mentor-pre-data-skills.png" alt="mentor skill data from pre survey"/>
</div>

#### Mentees

**What skills do you hope this program will help you develop and/or strengthen?**

<div style="text-align: center;display: block;">
<img class="benImg" src="mentee-pre-data-skills.png" alt="mentee skill data from pre survey"/>
</div>


