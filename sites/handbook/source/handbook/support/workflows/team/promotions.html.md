---
layout: handbook-page-toc
title: Promotions
category: Support Team
description: Process for submitting a promotions request
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

This page supplements the [Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/)
and documents promotions-related workflows and processes specific to the
Customer Support department.

## Process

The general process for promotions is:

1. Support team member completes promotion document in collaboration with their
   manager.
1. The manager circulates the promotion document for review and feedback
   amongst the manager's peers.
1. Support Directs review and calibrate all proposed promotions once a quarter.
1. Approved promotions proceed through the rest of the process.

## Promotion document

The promotion document template can be found in the
[Pillars section of the Promotions and Transfers handbook page](/handbook/people-group/promotions-transfers/#pillars).

Business Results and Business Justifications section should closely align with
the expected competencies for the role as laid out in the
[Support Career Framework](/handbook/engineering/career-development/matrix/engineering/support/).
