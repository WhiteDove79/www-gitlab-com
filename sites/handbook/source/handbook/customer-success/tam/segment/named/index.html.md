---
layout: handbook-page-toc
title: "TAM Segment: Named*"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ * This information is under active development and iteration, including but not limited to the name of the team, `Named`. This page will be updated on an ongoing basis as the team continues to grow and scale.
{: .alert .alert-warning}

## Overview

Definition: Named* (temporary designation) TAM on the account, product usage data-based success goals per account, programmatic enablement.

## Engagement Model

![Named Customer Lifecycle Management](/images/handbook/customer-success/Customer_Lifecycle_Journey_Named_TAM.png)



## Metrics

### Align

Success Plans are more focused on quantitative objectives such as use case adoption, tied into value drivers as determined in the pre-sales process, to ensure platform value is realized. Progress and success against these objectives are reported upon in the EBRs that will be held with those customers that have product usage data available.

#### Metrics for Align

1. 100% of net-new customers with green success plans (value drivers, use cases) within first 60 days
1. 100% of Priority 1 customers with green success plans
1. EBRs held with at least 50% of Priority 1 customers (with the eventual goal of 100%)

### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with TAM touchpoints throughout the first 60 days to ensure the customer is set up for success and has overcome initial roadblocks to adoption and value. TAM provides enablement webinars and adhoc sessions to ensure the customer has key adoption questions answered and has the best practice guidance needed to be successful. Use Case Health Scores enable the TAM to track the efficacy of these programs and determine strategic reach-outs.

#### Metrics for Enable

Onboarding:

1. Time to first engage: within 14 days
1. Time to 1st Value: within 30 days
1. Time to Onboard: within 45 days
1. Attendance in onboarding webinars of at least 50% of net-new customers
1. Average onboarding NPS & CSAT scores over 4.0 (out of 5)
1. Primary use case health scores green within first 90 days

### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity.  Webinars are the primary means of driving interest for customers into new segments, the success of which is tracked through customer engagement scorecards and product usage data insights around new use cases adopted. Low-license utilization reports will focus the TAM on identifying those customers at risk of contraction. The renewal NPS/CSAT survey 110 days before renewal enables the TAM to identify customers that may be challenged at the point of renewal and are not easily identifiable as challenged through product usage data.

#### Metrics for Expand & Renew

1. Consistent enablement and expansion webinars and attendance
   1. 3 webinars per quarter
   1. 80% of attendees stay for entire webinar
   1. Average feedback score of webinars over 4.0 (out of 5)
1. Consistent stage adoption efforts and adoption
   1. 50% of accounts have an open and active stage adoption play
   1. At least two stages are adopted for 90% of customers
   1. 10% increase of stages adopted across customer base quarter over quarter
   1. Initial adoption of a stage (10% of users) within 60 days of enablement
   1. Broad adoption of a stage (50% of users) within 6 months of enablement
1. % Low License Utilizations 'Saves' (improvement)
1. Renewal NPS & CSAT Scores over 4.0 (out of 5)


## Rhythm of Business

While the motions in the [rhythm of business](https://about.gitlab.com/handbook/customer-success/tam/rhythm/) will remain the same as the Strategic segment, the [Named](/handbook/customer-success/tam/segment/named/) TAM team will have a slightly different approach to their workflow and books of business.

A breakdown of what their expected tasks and hours spent for each can be found below, including a breakdown of those hours at a weekly, monthly, quarterly, and yearly perspective.

Please note this is just a guide to help TAMs prioritize and calibrate, to ensure they are making the most out of their time and can meet their goals and deliverables. Tasks and priorities, as well as the amount of time spent, can and will shift. TAMs should regularly work with their manager to ensure they have a healthy work/life balance and feel confident they can both complete all of their work as well as work towards continuous improvement.

| Tasks | Weekly | Monthly | Quarterly | Yearly | Notes |
|-------|--------|---------|-----------|--------|-------|
| [Cadence calls](/handbook/customer-success/tam/cadence-calls/) | 8 | 35 | 105 | 420 | 1 hour per customer (30 minutes for the call, 30 minutes for prep & follow up) |
| [Customer onboarding](/handbook/customer-success/tam/onboarding/) | 1 | 4 | 12 | 48 | 1 hour per week per customer for average of 3 customers/quarter |
| [EBRs](/handbook/customer-success/tam/ebr/) | 2 | 9 | 27 | 108 | Average of 2 customer EBRs per month, with 4 hours for each (1 hour for call, 3 hours prep) |
| Account planning | 3 | 13 | 39 | 156 | [Success planning](/handbook/customer-success/tam/success-plans/), long-term strategy, [expansion opportunities](/handbook/customer-success/tam/stage-enablement-and-expansion/), etc. |
| [Webinars & workshops](/handbook/customer-success/tam/workshops/) | 1 | 4 | 12 | 48 | 1 webinar and 1 workshops per month, 2 hours each (1 hour for call, 1 hour prep) |
| Adhoc customer requests | 2 | 9 | 27 | 108 | Responding to customer emails and questions |
| Customer outreach | 2 | 9 | 27 | 108 | Reaching out to customers proactively (upcoming webinars, features, etc.) |
| [Gainsight work](/handbook/customer-success/tam/gainsight/) | 2 | 9 | 27 | 108 | Logging updates and maintenance |
| Special projects | 3 | 13 | 39 | 156 | [OKRs](https://gitlab.com/gitlab-com/customer-success/okrs/-/issues), team- and org-level initiatives, etc. |
| [Professional development](/handbook/people-group/learning-and-development/) | 3 | 13 | 39 | 156 | GitLab product learning, courses, programs, certifications, etc. |
| [Renewals](/handbook/customer-success/tam/renewals/) | 1 | 4 | 12 | 48 | Roughly 1 hour per customer for annual renewal |
| Internal meetings |  3 | 13 | 39 | 156 | Team meetings, [1:1s](/handbook/leadership/1-1/), [account team](/handbook/customer-success/account-team/) syncs, etc. |
| Review [release blog posts](/releases/categories/releases/) | 0.25 | 1 | 3 | 12 | 1 hour per release reviewing new features & functionality as well as deprecations |
| **Total hours spent** | 31.25 | 136 | 408 | 1632 |  |

In addition to the work spent for each task, this framework assumes team members will take 56 days or 448 hours of [PTO](/handbook/paid-time-off/) per year, which includes holidays, [Family & Friends days](/company/family-and-friends-day/), vacation, sick time, etc., but this is not a requirement, and team members are able and encouraged to take as much PTO as they'd like, as long as they follow our company and [team](/handbook/customer-success/tam/pto/) guidelines.

